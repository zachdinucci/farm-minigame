﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

/// <summary>
/// State of AI agent
/// </summary>
public enum STATE
{
    IDLE,
    PURSUE,
    PURSUE_PLANT,
    ATTACK,
    FLEE,
    WANDER,
    DEPOSITING,
    ENRAGED,
    SLEEP,
}

public enum EVENT
{
    ENTER,
    UPDATE,
    EXIT
}

/// <summary>
/// Base state
/// </summary>
public class State
{
    public STATE stateName;
    protected EVENT stage;
    protected GameObject npc;
    protected Animator anim;
    protected Transform player;
    protected State nextState;
    protected NavMeshAgent agent;
    protected AI ai;

    float visibleDistance = 8;
    float visibleAngle = 60.0f;
    float attackDistance = 1.0f;

    public State(GameObject _npc, NavMeshAgent _agent, Animator _anim, Transform _player, AI _ai)
    {
        npc = _npc;
        agent = _agent;
        //anim = _anim;
        stage = EVENT.ENTER;
        player = _player;
        ai = _ai;

        visibleDistance = ai.viewDistance;
        visibleAngle = ai.viewAngle;
        attackDistance = ai.attackDistance;
    }
    public virtual void Enter() { stage = EVENT.UPDATE; }
    public virtual void Update() { stage = EVENT.UPDATE; }
    public virtual void Exit() { stage = EVENT.EXIT; }

    /// <summary>
    /// Called in States update
    /// </summary>
    /// <returns>returns state</returns>
    public State Process()
    {
        if (stage == EVENT.ENTER) Enter();
        if (stage == EVENT.UPDATE) Update();
        if (stage == EVENT.EXIT)
        {
            Exit();
            return nextState;
        }
        return this;
    }

    /// <summary>
    /// Returns whether the agent can or cannot see the player
    /// </summary>
    /// <returns>visibility status</returns>
    public bool CanSeePlayer()
    {
        Vector3 dir = player.position - npc.transform.position;
        float angle = Vector3.Angle(dir, npc.transform.forward);
        if (dir.magnitude < visibleDistance && angle < visibleAngle)
            return true;
        return false;
    }

    /// <summary>
    /// Returns wheter the agent can or cannot attack the player
    /// </summary>
    /// <returns>attack status</returns>
    public bool CanAttack()
    {
        Vector3 dir = player.position - npc.transform.position;
        if (dir.magnitude < attackDistance)
            return true;
        return false;
    }

    /// <summary>
    /// Returns current agent state
    /// </summary>
    /// <returns>current state</returns>
    public STATE GetCurrentAIState()
    {
        return stateName;
    }
}

