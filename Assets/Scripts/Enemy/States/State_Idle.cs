﻿using UnityEngine;
using UnityEngine.AI;

/// <summary>
/// Agent Idle behavior
/// </summary>
public class State_Idle : State
{
    public State_Idle(GameObject _npc, NavMeshAgent _agent, Animator _anim, Transform _player, AI _ai) : base(_npc, _agent, _anim, _player, _ai)
    {
        stateName = STATE.IDLE;
    }

    public override void Enter()
    {
        //anim.SetTrigger("isIdle");
        agent.isStopped = true;
        base.Enter();
    }

    public override void Update()
    {
        if (CanSeePlayer())
        {
            nextState = new State_Pursue(npc, agent, anim, player, ai);
            stage = EVENT.EXIT;
        }
        else if (Random.Range(0, 100) < 10)
        {
            nextState = new State_Wander(npc, agent, anim, player, ai);
            stage = EVENT.EXIT;
        }
    }

    public override void Exit()
    {
        //anim.ResetTrigger("isIdle");
        base.Exit();
    }
}
