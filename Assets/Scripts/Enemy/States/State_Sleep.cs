﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

/// <summary>
/// Agent Sleep behavior
/// </summary>
public class State_Sleep : State
{
    float wanderRange = 20f;
    NavMeshHit navHit;
    Vector3 wanderTarget;
    float checkRate;
    float nextCheck;
    float sleepTimer = 5f;
    bool isAsleep = true;

    public State_Sleep(GameObject _npc, NavMeshAgent _agent, Animator _anim, Transform _player, AI _ai) : base(_npc, _agent, _anim, _player, _ai)
    {
        stateName = STATE.SLEEP;
        agent.isStopped = false;
        agent.speed = ai.walkSpeed;
        sleepTimer = ai.sleepDuration;
    }

    public override void Enter()
    {
        //anim.SetTrigger("isAttacking");
        base.Enter();
        checkRate = Random.Range(0.3f, 0.4f);
        isAsleep = true;
        ai.PlayAudioClip(ai.oinkClip);

    }

    public override void Update()
    {
        //Wander behavior - counts time until next check
        if (Time.time > nextCheck)
        {
            nextCheck = Time.time + checkRate;
            CheckIfShouldWander();
        }

        //Time to be asleep
        if(sleepTimer > 0)
        {
            sleepTimer -= Time.deltaTime;
        }
        else
        {
            isAsleep = false;
            CheckIfShouldWander();
        }
    }

    public override void Exit()
    {
        //anim.ResetTrigger("isAttacking");
        base.Exit();

    }

    //Check if agent should wander/sleep or exit
    void CheckIfShouldWander()
    {
        if (isAsleep)
        {
            if (RandomWanderTarget(player.transform.position, wanderRange, out wanderTarget))
                agent.SetDestination(wanderTarget);
        }
        else if (!isAsleep)
        {
            if (CanSeePlayer())
            {
                nextState = new State_Pursue(npc, agent, anim, player, ai);
                stage = EVENT.EXIT;
            }
            else
            {
                nextState = new State_Wander(npc, agent, anim, player, ai);
                stage = EVENT.EXIT;
            }
        }
    }

    //Returns availability of navmesh target
    bool RandomWanderTarget(Vector3 center, float range, out Vector3 result)
    {
        Vector3 randomPoint = center + Random.insideUnitSphere * wanderRange;
        if (NavMesh.SamplePosition(randomPoint, out navHit, 5.0f, NavMesh.AllAreas))
        {
            result = navHit.position;
            return true;
        }
        else
        {
            result = center;
            return false;
        }
    }
}
