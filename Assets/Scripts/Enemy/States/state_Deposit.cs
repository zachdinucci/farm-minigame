﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

/// <summary>
/// Agent Deposit behavior
/// </summary>
public class state_Deposit : State
{
    public state_Deposit(GameObject _npc, NavMeshAgent _agent, Animator _anim, Transform _player, AI _ai) : base(_npc, _agent, _anim, _player, _ai)
    {
        stateName = STATE.DEPOSITING;
        agent.speed = ai.fleeSpeed;
        agent.isStopped = false;
    }

    public override void Enter()
    {
        //anim.SetTrigger("isWalking");
        base.Enter();
        ai.PlayAudioClip(ai.oinkClip);
    }

    public override void Update()
    {
        //If score is greater than 0 --> find a goal to deposit too
        if (ai.GetHeldScore() >= 1)
        {
            agent.SetDestination(GoalManager.Instance.GetNearestGoal(agent.transform.position));
        }

        //If score is 0 at this point --> sleep
        if (ai.GetHeldScore() <= 0)
        {
            ai.PlayAudioClip(ai.oinkClip);
            nextState = new State_Sleep(npc, agent, anim, player, ai);
            stage = EVENT.EXIT;
        }
        
    }

    public override void Exit()
    {
        //anim.ResetTrigger("isWalking");
        base.Exit();
    }
}
