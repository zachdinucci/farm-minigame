﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

/// <summary>
/// Agent Enraged behavior
/// </summary>
public class State_Enraged : State
{
    Vector3 originalScale;
    float enrageTimer = 8f;

    public State_Enraged(GameObject _npc, NavMeshAgent _agent, Animator _anim, Transform _player, AI _ai) : base(_npc, _agent, _anim, _player, _ai)
    {
        stateName = STATE.ENRAGED;
        agent.speed = ai.chaseSpeed;
        agent.isStopped = false;
        enrageTimer = Random.Range(ai.enragedDuration.x, ai.enragedDuration.y);
    }

    public override void Enter()
    {
        //anim.SetTrigger("isWalking");
        base.Enter();
        originalScale = agent.transform.localScale;
        agent.transform.localScale = new Vector3(1.2f, 1.2f, 1.2f);
        ai.SetEnraged(true);

        ai.PlayAudioClip(ai.enrageClip);
    }

    public override void Update()
    {
        //Time to be enraged or exit
        if(enrageTimer > 0)
            enrageTimer -= Time.deltaTime;
        else
        {
            nextState = new State_Sleep(npc, agent, anim, player, ai);
            stage = EVENT.EXIT;
        }

        // the ai will look for you.. it will never get tired... it will never get hungry... 
        // to the last breath you take.. the ai will hunt you down till the
        // end of time.... unless you leave the navmesh lol stupid ai.. oh god im tired
        agent.SetDestination(player.position);
        if (agent.hasPath)
        {
            if (CanAttack())
            {
                nextState = new State_Attack(npc, agent, anim, player, ai);
                stage = EVENT.EXIT;
            }
        }
    }

    public override void Exit()
    {
        //anim.ResetTrigger("isWalking");
        base.Exit();
        ai.SetEnraged(false);
        agent.transform.localScale = originalScale;
    }
}
