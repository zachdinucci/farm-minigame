﻿using UnityEngine;
using UnityEngine.AI;

/// <summary>
/// Agent Flee behavior
/// </summary>
public class State_Flee : State
{
    public State_Flee(GameObject _npc, NavMeshAgent _agent, Animator _anim, Transform _player, AI _ai) : base(_npc, _agent, _anim, _player, _ai)
    {
        stateName = STATE.FLEE;
        agent.isStopped = false;
        agent.speed = ai.fleeSpeed;
    }

    public override void Enter()
    {
        //anim.SetTrigger("isAttacking");
        base.Enter();
        ai.PlayAudioClip(ai.deathClip);

    }

    public override void Update()
    {
        //If agent is near player - move oposite direciton 
        float distance = Vector3.Distance(agent.transform.position, player.transform.position);

        //Flee behavior
        if (distance < 5 && distance > 0.00f)
        {
            Vector3 dirToPlayer = agent.transform.position - player.transform.position;
            Vector3 newPos = agent.transform.position + dirToPlayer;
            agent.SetDestination(newPos);
        }
        //Deposit score if safe and able
        else if (distance > 5 && ai.GetHeldScore() >= 1)
        {
            nextState = new state_Deposit(npc, agent, anim, player, ai);
            stage = EVENT.EXIT;
        }
        //Wander if safe
        else if (distance > 5)
        {
            nextState = new State_Wander(npc, agent, anim, player, ai);
            stage = EVENT.EXIT;
        }
    }

    public override void Exit()
    {
        //anim.ResetTrigger("isAttacking");
        base.Exit();
    }
}
