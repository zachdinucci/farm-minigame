﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

/// <summary>
/// Agent Pursue Plant behavior
/// </summary>
public class State_PursuePlant : State
{
    float searchTimeout = 10f;
    bool hasTarget = false;
    crops tempCrop = null;
    Vector3 tempPos;

    public State_PursuePlant(GameObject _npc, NavMeshAgent _agent, Animator _anim, Transform _player, AI _ai) : base(_npc, _agent, _anim, _player, _ai)
    {
        stateName = STATE.PURSUE_PLANT;
        agent.speed = ai.chaseSpeed;
        agent.isStopped = false;
    }

    public override void Enter()
    {
        //anim.SetTrigger("isWalking");
        base.Enter();
        PlantManager.Instance.PlantRecalculated += Recalc;
    }

    //Called if a plant is removed
    private void Recalc()
    {
        hasTarget = false;
    }

    //If there is no target, find one, or else move on with life.
    private void TargetCheck()
    {
        if (hasTarget) return;

        if (PlantManager.Instance.IsThereAHarvestablePlant())
        {
            tempCrop = PlantManager.Instance.GetNearestHarvestablePlant(agent.transform.position);
            tempPos = tempCrop.transform.position;
            hasTarget = true;
        }
    }

    public override void Update()
    {
        if (PlantManager.Instance.IsThereAHarvestablePlant() == true && ai.GetHeldScore() <= 0)
        {
            TargetCheck();

            //If AI goes poo brain --> exit
            if (searchTimeout > 0)
                searchTimeout -= Time.deltaTime;
            else
            {
                nextState = new State_Wander(npc, agent, anim, player, ai);
                stage = EVENT.EXIT;
            }

            float distance;
            agent.SetDestination(tempCrop.transform.position);
            distance = Vector3.Distance(agent.transform.position, tempPos);

            ai.SetPlantTargetID("Target: ", tempCrop.GetID());

            if (distance < 0.5f)
            {
                ai.PlayAudioClip(ai.attackClip);
                //If plant is in range - eat it
                tempCrop.AnnihilateThisBiznitch("Enemy");
                ai.AddHeldScore(1);

                //If score is above 0 --> deposit
                if (ai.GetHeldScore() >= 1)
                {
                    nextState = new state_Deposit(npc, agent, anim, player, ai);
                    stage = EVENT.EXIT;
                    ai.SetPlantTargetID("Target: N/A");
                }
                else
                {
                    nextState = new State_Wander(npc, agent, anim, player, ai);
                    stage = EVENT.EXIT;
                    ai.SetPlantTargetID("Target: N/A");
                }
            }
        }
        else
        {
            nextState = new State_Wander(npc, agent, anim, player, ai);
            stage = EVENT.EXIT;
        }
    }

    public override void Exit()
    {
        PlantManager.Instance.PlantRecalculated -= Recalc;
        hasTarget = false;
        //anim.ResetTrigger("isWalking");
        base.Exit();
    }


}
