﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

/// <summary>
/// Agent Pursue behavior
/// </summary>
public class State_Pursue : State
{
    public State_Pursue(GameObject _npc, NavMeshAgent _agent, Animator _anim, Transform _player, AI _ai) : base(_npc, _agent, _anim, _player, _ai)
    {
        stateName = STATE.PURSUE;
        agent.speed = ai.chaseSpeed;
        agent.isStopped = false;
    }

    public override void Enter()
    {
        //anim.SetTrigger("isWalking");
        base.Enter();
    }

    public override void Update()
    {
        //Set agent target to player --> if able to attack, attack - else --> wander.
        agent.SetDestination(player.position);
        if (agent.hasPath)
        {
            if (CanAttack())
            {
                nextState = new State_Attack(npc, agent, anim, player, ai);
                stage = EVENT.EXIT;
            }
            else if (!CanSeePlayer())
            {
                nextState = new State_Wander(npc, agent, anim, player, ai);
                stage = EVENT.EXIT;
            }
        }
    }

    public override void Exit()
    {
        //anim.ResetTrigger("isWalking");
        base.Exit();
    }

}
