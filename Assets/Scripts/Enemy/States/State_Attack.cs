﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

/// <summary>
/// Agent Attack behavior
/// </summary>
public class State_Attack : State
{
    float rotationSpeed = 10.0f;

    public State_Attack(GameObject _npc, NavMeshAgent _agent, Animator _anim, Transform _player, AI _ai) : base(_npc, _agent, _anim, _player, _ai)
    {
        stateName = STATE.ATTACK;
    }

    public override void Enter()
    {
        //anim.SetTrigger("isAttacking");
        agent.isStopped = true;

        //If player has held score - take 1 and add it to own held score - inflict damage
        if (GoalManager.Instance.playerHeldScore > 0)
        {
            ai.PlayAudioClip(ai.stealClip);
            ai.AddHeldScore(1);
            GoalManager.Instance.PlayerAddHeld(-1);
        }
        else
            ai.PlayAudioClip(ai.attackClip);

        player.GetComponent<PlayerControl>().AddHealth(-(ai.attackDamage));
        base.Enter();
    }

    public override void Update()
    {
        //Look at target
        Vector3 dir = player.position - npc.transform.position;
        float angle = Vector3.Angle(dir, npc.transform.forward);
        npc.transform.rotation = Quaternion.Slerp(npc.transform.rotation, Quaternion.LookRotation(dir), Time.deltaTime * rotationSpeed);

        nextState = new State_Flee(npc, agent, anim, player, ai);
        stage = EVENT.EXIT;
    }

    public override void Exit()
    {
        //anim.ResetTrigger("isAttacking");
        base.Exit();
    }
}
