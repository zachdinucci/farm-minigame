﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

/// <summary>
/// Agent Wander behavior
/// </summary>
public class State_Wander : State
{
    float wanderRange = 20f;
    NavMeshHit navHit;
    Vector3 wanderTarget;
    float checkRate;
    float nextCheck;
    float wanderTimer;
    float timer;
    bool canEat = false;

    public State_Wander(GameObject _npc, NavMeshAgent _agent, Animator _anim, Transform _player, AI _ai) : base(_npc, _agent, _anim, _player, _ai)
    {
        stateName = STATE.WANDER;
        agent.isStopped = false;
        agent.speed = ai.walkSpeed;
    }

    public override void Enter()
    {
        //anim.SetTrigger("isWalking");
        base.Enter();
        checkRate = Random.Range(0.3f, 0.4f);
        wanderTimer = Random.Range(3f, 6f);
        ai.PlayAudioClip(ai.oinkClip);
    }

    public override void Update()
    {
        //Count til next wander check - prevent generation of new target/frame
        if (Time.time > nextCheck)
        {
            nextCheck = Time.time + checkRate;
            CheckIfShouldWander();
        }

        //Prevent harvesting plant if just harvested
        if (Time.time > timer)
        {
            timer = Time.time + wanderTimer;
            canEat = true;
        }
    }

    public override void Exit()
    {
        //anim.ResetTrigger("isWalking");
        base.Exit();

    }

    //Checks if agent should wander or break out of state
    void CheckIfShouldWander()
    {
        //If cant see player, no harvestbale plant, and score less than 1 --> wander
        if (!CanSeePlayer() && !PlantManager.Instance.IsThereAHarvestablePlant() && ai.GetHeldScore() < 1)
        {
            

            if (RandomWanderTarget(player.transform.position, wanderRange, out wanderTarget))
                agent.SetDestination(wanderTarget);
        }
        //If score is 1 or higher --> deposit
        else if (ai.GetHeldScore() >= 1)
        {
            nextState = new state_Deposit(npc, agent, anim, player, ai);
            stage = EVENT.EXIT;
        }
        //If score is 0, plant is available, can eat, and health is above 51 --> seek plant
        else if (ai.GetHeldScore() <= 0 && PlantManager.Instance.IsThereAHarvestablePlant() && canEat && ai.health >= 51)
        {
            nextState = new State_PursuePlant(npc, agent, anim, player, ai);
            stage = EVENT.EXIT;
        }
        //If player is visible, score is 0, and health is above 51 --> pursue player
        else if (CanSeePlayer() && ai.GetHeldScore() <= 0 && ai.health >= 51)
        {
            nextState = new State_Pursue(npc, agent, anim, player, ai);
            stage = EVENT.EXIT;
        }
        //If health is less than 50 --> enraged state
        else if (ai.health <= 50)
        {
            nextState = new State_Enraged(npc, agent, anim, player, ai);
            stage = EVENT.EXIT;
        }

    }

    //Return availability of generated vec3 target for agent destination
    bool RandomWanderTarget(Vector3 center, float range, out Vector3 result)
    {
        Vector3 randomPoint = center + Random.insideUnitSphere * wanderRange;
        if (NavMesh.SamplePosition(randomPoint, out navHit, 5.0f, NavMesh.AllAreas))
        {
            result = navHit.position;
            return true;
        }
        else
        {
            result = center;
            return false;
        }
    }


}
