﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using TMPro;
using UnityEngine.Animations;

/// <summary>
/// Controller for AI agents
/// </summary>
public class AI : MonoBehaviour
{
    #region Variables
    [Header("Gloabal Target")]
    public Transform player;

    [Header("AI Statistics")]
    public TextMeshProUGUI stateText;
    public TextMeshProUGUI scoreText;
    public TextMeshProUGUI plantTargetIDText;
    public TextMeshProUGUI enemyHealthText;

    [Header("DEBUG SCORE")]
    public int heldScore = 0;

    [Header("AI Settings")]
    public string aIName = "Default Enemy";
    public float health = 100;
    public float walkSpeed = 2f;
    public float chaseSpeed = 4f;
    public float fleeSpeed = 3f;
    public float attackDamage = 10f;
    public float viewDistance = 4f;
    public float viewAngle = 60.0f;
    public float attackDistance = 1.0f;
    public float sleepDuration = 5f;
    public float armorMultiplier = 1f;
    public Vector2 enragedDuration = new Vector2(8f, 12f);

    [Header("Extras")]
    public List<MeshRenderer> eyes;
    public Material enragedMaterial;
    public Material baesMaterial;
    public Light enrageLight;

    //Last minute thingymajig
    [Header("Audio Clips")]
    public AudioClip oinkClip;
    public AudioClip deathClip;
    public AudioClip stealClip;
    public AudioClip attackClip;
    public AudioClip fleeClip;
    public AudioClip enrageClip;

    private bool hadDied = false;

    Collider col;
    NavMeshAgent agent;
    Animator anim;
    State currentState;
    AI ai;

    [Header("DEBUG STATE")]
    [SerializeField]
    private STATE state = STATE.IDLE;

    private AudioSource audSource;

    #endregion Variables

    void Start()
    {
        //Initialize Values
        player = FindObjectOfType<PlayerControl>().transform;
        agent = this.GetComponent<NavMeshAgent>();
        ai = this.GetComponent<AI>();
        anim = this.GetComponent<Animator>();
        col = this.GetComponent<Collider>();
        audSource = this.GetComponent<AudioSource>();
        currentState = new State_Idle(this.gameObject, agent, anim, player, ai);
        SetPlantTargetID("Target: N/A");
        enemyHealthText.SetText(health + "%");
        SetEnraged(false);

        //Initialize AI Canvas Look At Constraints
        LookAtConstraint lk = GetComponentInChildren<LookAtConstraint>();
        ConstraintSource lkAt = new ConstraintSource();
        lkAt.weight = 1;
        lkAt.sourceTransform = Camera.main.transform;
        lk.SetSource(0, lkAt);
    }

    void Update()
    {
        //This can be optimized but it works
        state = currentState.GetCurrentAIState();
        if (stateText != null) stateText.SetText(state.ToString());
        if (scoreText != null) scoreText.SetText("HELD: " + agent.GetComponent<AI>().GetHeldScore());
        currentState = currentState.Process();
    }

    /// <summary>
    /// Plays a supplied audio clip
    /// </summary>
    /// <param name="clip">audio clip</param>
    public void PlayAudioClip(AudioClip clip)
    {
        audSource.PlayOneShot(clip);
        audSource.clip = null;
    }

    /// <summary>
    /// Kill and destroy the AI agent
    /// </summary>
    void Die()
    {
        if (!hadDied)
        {
            hadDied = true;
            GoalManager.Instance.PlayerAddHeld(heldScore);
            agent.isStopped = true;
            GoalManager.Instance.AddToEnemiesKilled();
            Destroy(gameObject);
        }
    }

    /// <summary>
    /// Set 'EXTRA' AI enraged features on or off based on bool
    /// </summary>
    /// <param name="value">Boolean to set true/false or on/off.</param>
    public void SetEnraged(bool value)
    {
        if (eyes == null) return;
        if (value)
        {
            foreach (MeshRenderer x in eyes)
            {
                if (enragedMaterial != null)
                    x.material = enragedMaterial;
            }
            if (enrageLight != null) enrageLight.enabled = true;
        }
        else
        {
            foreach (MeshRenderer x in eyes)
            {
                if (baesMaterial != null)
                    x.material = baesMaterial;
            }
            if (enrageLight != null) enrageLight.enabled = false;
        }
    }

    /// <summary>
    /// Add held score of agent.
    /// </summary>
    /// <param name="value">Score to be added</param>
    public void AddHeldScore(int value)
    {
        heldScore += value;
    }

    /// <summary>
    /// Set base held score of agent.
    /// </summary>
    /// <param name="value">Score to be set to</param>
    public void SetHeldScore(int value)
    {
        heldScore = value;
    }

    /// <summary>
    /// Get the agents held score
    /// </summary>
    /// <returns>held score</returns>
    public int GetHeldScore()
    {
        return heldScore;
    }

    /// <summary>
    /// Sets the agents plant target value on canvas
    /// </summary>
    /// <param name="t">textbox output</param>
    /// <param name="id">plant or target id</param>
    public void SetPlantTargetID(string t, int id)
    {
        plantTargetIDText.SetText(t + id);
    }

    /// <summary>
    /// Sets the agents plant target value on canvas
    /// </summary>
    /// <param name="t">textbox output</param>
    public void SetPlantTargetID(string t)
    {
        if (plantTargetIDText != null) plantTargetIDText.SetText(t);
    }

    /// <summary>
    /// Inflict damage upon agent
    /// </summary>
    /// <param name="value">damage amount</param>
    public void TakeDamage(float value)
    {
        if (armorMultiplier <= 0) armorMultiplier = 1;
        float tempDamage = 0;
        tempDamage = value / armorMultiplier;
        health -= tempDamage;
        enemyHealthText.SetText(health + "%");
        if (health <= 0) Die();
    }
}
