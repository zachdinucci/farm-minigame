﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class mainMenuManager : MonoBehaviour
{
    public TMP_InputField desiredName;
    public Button button;
    public TextMeshProUGUI errorText;
    private string _name = "Default";

    private float errorDelay = 4f;
    private bool errorTriggered = false;

    // Start is called before the first frame update
    void Start()
    {
        errorText.SetText("");
        desiredName.characterLimit = 12;
    }

    // Update is called once per frame
    void Update()
    {
        if (desiredName.isActiveAndEnabled && Input.GetKeyDown(KeyCode.Return))
            SetName();

        if(errorTriggered && errorDelay > 0)
            errorDelay -= Time.deltaTime;
        else
        {
            errorTriggered = false;
            errorText.SetText("");
        }

        if (Input.GetKeyDown(KeyCode.Escape)) QuitGame();
    }

    public void SetName()
    {
        if(desiredName.text == "")
        {
            errorTriggered = true;
            errorText.SetText("PLEASE ENTER A NAME");
        }
        else
        {
            _name = desiredName.text.ToUpper().ToString();
            PlayerPrefs.SetString("userName", _name);
            StartGame();
        }
    }

    public void StartGame()
    {
        SceneManager.LoadScene("Main Scene");
    }

    public void QuitGame()
    {
        Debug.Log("Quitting...");
        Application.Quit();
    }
    
}
