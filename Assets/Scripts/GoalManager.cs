﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Goal Manager Singleton
/// </summary>
public class GoalManager : MonoBehaviour
{
    public static GoalManager Instance { get; private set; } // Static singleton

    public int playerScore = 0;
    public int enemyScore = 0;
    public int playerHeldScore = 0;
    public int enemiesKilled = 0;
    public List<Transform> goals;

    private TimeSpan timePlaying;
    private bool timerActive;
    private float elapsedTime;
    private string timeCount = "00:00:00";
    private bool coutroutineActive = false;


    private void Awake()
    {
        if (Instance == null) { Instance = this; }
        else { Destroy(gameObject); }

        ToggleTimer(true);
    }

    /// <summary>
    /// Toggle on or off a system counter
    /// </summary>
    /// <param name="value">true/false --> on/off</param>
    /// <returns>returns status</returns>
    public bool ToggleTimer(bool value)
    {
        //This is balls but again its late and as Todd Howard said - "It just works"
        if (value && !coutroutineActive)
        {
            timerActive = value;
            coutroutineActive = true;
            StartCoroutine(UpdateTimer());
        }else if(!value && coutroutineActive)
        {
            timerActive = value;
            coutroutineActive = false;
            StopCoroutine(UpdateTimer());
        }
        return value;
    }

    //Count while timerActive is true
    private IEnumerator UpdateTimer()
    {
        while (timerActive)
        {
            elapsedTime += Time.deltaTime;
            timePlaying = TimeSpan.FromSeconds(elapsedTime);
            string timePlayingStr = timePlaying.ToString("mm':'ss'.'ff");
            timeCount = timePlayingStr;
            yield return null;
        }
    }

    /// <summary>
    /// Returns the nearest vec3 of an object in the goals array
    /// </summary>
    /// <param name="pos">current position</param>
    /// <returns>target position</returns>
    public Vector3 GetNearestGoal(Vector3 pos)
    {
        GameObject tempObject = null;
        if (goals != null)
        {
            float minDistance = float.MaxValue;
            for (int i = 0; i < goals.Count; i++)
            {
                if (Vector3.Distance(pos, goals[i].transform.position) < minDistance)
                {
                    minDistance = Vector3.Distance(pos, goals[i].transform.position);
                    tempObject = goals[i].gameObject;
                }
            }
        }
        return tempObject.transform.position;
    }

    /// <summary>
    /// Get score of player
    /// </summary>
    /// <returns>player score</returns>
    public int GetPlayerScore()
    {
        return playerScore;
    }
    /// <summary>
    /// Get score of enemy
    /// </summary>
    /// <returns>enemy score</returns>
    public int GetEnemyScore()
    {
        return enemyScore;
    }
    /// <summary>
    /// Get amount of plants player holds
    /// </summary>
    /// <returns>amount of held plants</returns>
    public int GetPlantsHeld()
    {
        return playerHeldScore;
    }
    /// <summary>
    /// Set player held score to a value
    /// </summary>
    /// <param name="value">score to set</param>
    public void SetPlayerHeldScore(int value)
    {
        playerHeldScore = value;
    }
    /// <summary>
    /// Add to enemy score
    /// </summary>
    /// <param name="value">score to add</param>
    public void EnemyAddScore(int value)
    {
        enemyScore += value;
        AudioManager.Instance.PlayAudioClip(AudioManager.Instance.enemyGoalClip);
    }
    /// <summary>
    /// Add to player held score
    /// </summary>
    /// <param name="value">score to add</param>
    public void PlayerAddHeld(int value)
    {
        playerHeldScore += value;
    }
    /// <summary>
    /// Add to player score
    /// </summary>
    /// <param name="value">score to add</param>
    public void PlayerAddScore(int value)
    {
        playerScore += value;
        AudioManager.Instance.PlayAudioClip(AudioManager.Instance.playerGoalClip);
    }
    /// <summary>
    /// Add player held score to player score and reset held score to 0
    /// </summary>
    public void DepositPlayerScore()
    {
        playerScore += playerHeldScore;
        playerHeldScore = 0;
    }
    /// <summary>
    /// Add to enemies killed value
    /// </summary>
    public void AddToEnemiesKilled()
    {
        enemiesKilled++;
    }
    /// <summary>
    /// Returns amount of enemeis killed
    /// </summary>
    /// <returns>enemies killed as int</returns>
    public int GetEnemiesKilled()
    {
        return enemiesKilled;
    }
    /// <summary>
    /// Returns system time as a formated string
    /// </summary>
    /// <returns>system time as formated string</returns>
    public string GetTime()
    {
        return timeCount;
    }

    public float GetElapsedTime()
    {
        return elapsedTime;
    }
}
