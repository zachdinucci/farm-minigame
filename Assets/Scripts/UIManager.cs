﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

/// <summary>
/// UI Manager Singleton
/// </summary>
public class UIManager : MonoBehaviour
{
    public static UIManager Instance { get; private set; } // Static singleton

    [Header("UI Text Mesh Elements")]
    public PlayerControl player;
    public TextMeshProUGUI playerScoreTMP;
    public TextMeshProUGUI enemyScoreTMP;
    public TextMeshProUGUI playerHealthTMP;
    public TextMeshProUGUI plantsHeldTMP;
    public TextMeshProUGUI enemiesKilledTMP;
    public TextMeshProUGUI timerTMP;
    public Slider healthBar;

    private int playerScore = 0;
    private int enemyScore = 0;
    private float playerHealth = 100;
    private int plantsHeld = 0;
    private int enemiesKilled = 0;
    private string timer;

    [Space]
    public HighScoreTable highScoreTable;

    private void Awake()
    {
        if (Instance == null) { Instance = this; }
        else { Destroy(gameObject); }

    }

    void Start()
    {
        Time.timeScale = 1;
        LoadName();
        UpdateUI(playerScore, enemyScore, playerHealth, plantsHeld, enemiesKilled);
    }

    void Update()
    {
        playerScore = GoalManager.Instance.GetPlayerScore();
        enemyScore = GoalManager.Instance.GetEnemyScore();
        plantsHeld = GoalManager.Instance.GetPlantsHeld();
        enemiesKilled = GoalManager.Instance.GetEnemiesKilled();
        playerHealth = player.GetHealth();
        timer = GoalManager.Instance.GetTime();
        if (player != null) playerHealth = player.GetHealth();
        UpdateUI(playerScore, enemyScore, playerHealth, plantsHeld, enemiesKilled);

        if (Input.GetKeyDown(KeyCode.Escape))
            Quit();
    }

    /// <summary>
    /// Recalcualtes UI statistics
    /// </summary>
    /// <param name="pScore">player score</param>
    /// <param name="eScore">enemy score</param>
    /// <param name="pHealth">player health</param>
    /// <param name="pHeld">plants held by player</param>
    /// <param name="eKilled">enemies killed</param>
    public void UpdateUI(int pScore, int eScore, float pHealth, int pHeld, int eKilled)
    {
        if (playerHealth <= 0) { playerHealth = 0; }
        UpdateHealthBar();

        playerScoreTMP.SetText("PLAYER SCORE: " + playerScore);
        enemyScoreTMP.SetText("ENEMY SCORE: " + enemyScore);
        plantsHeldTMP.SetText("PLANTS HELD: " + plantsHeld);
        playerHealthTMP.SetText("PLAYER HEALTH: " + playerHealth + "%");
        enemiesKilledTMP.SetText("ENEMIES KILLED: " + enemiesKilled);
        timerTMP.SetText(timer);
    }

    private void LoadName()
    {
        name = PlayerPrefs.GetString("userName");
        Debug.Log(name + " set as username!");
    }

    //private void SetMaxHealth()
    //{
    //    healthBar.maxValue = playerHealth;
    //    healthBar.value = playerHealth;
    //}

    private void UpdateHealthBar()
    {
        healthBar.value = playerHealth;
    }

    public void EndGame()
    {
        if (name == null || name == "") name = "NO NAME";
        highScoreTable.gameObject.SetActive(true);
        int score = (int)(player.health * enemiesKilled * playerScore + GoalManager.Instance.GetElapsedTime()) / (enemyScore + 1);
        highScoreTable.AddHighScoreEntry(score, timer, playerScore, enemyScore, enemiesKilled, name);
        highScoreTable.Initialize();
        Time.timeScale = 0;
    }

    public void Restart()
    {
        SceneManager.LoadScene("MainMenu");
    }

    public void Quit()
    {
        Application.Quit();
    }
}
