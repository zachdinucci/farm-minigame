﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public enum cropStates
{
    Ready,
    Growing,
    Grown,
    Parched,
    Destroyed
}

/// <summary>
/// Crop behavior script
/// </summary>
public class crops : MonoBehaviour
{
    [Header("Crop Settings")]
    public bool startAsGrowning = false;
    public float stageSpeed = 5f;
    public GameObject player;
    public GameObject[] plantStages;
    public int id;

    [Header("Crop UI Settings")]
    public TextMeshProUGUI pressEText; 

    [Header("DEBUG STATE")]
    [SerializeField]
    private cropStates crop_State;

    private bool interact = false;
    private float elapsed;
    private int currentStage = 0;

    void Start()
    {
        crop_State = cropStates.Ready;
        if (startAsGrowning)
        {
            ReadyMode();
            interact = true;
        }
    }

    void Update()
    {
        elapsed += Time.deltaTime;
        //Account and execute stages of plant growth
        if (crop_State == cropStates.Ready)
            ReadyMode();
        else if (crop_State == cropStates.Growing)
            GrowingMode();
        else if (crop_State == cropStates.Grown)
            GrownMode();
        else if (crop_State == cropStates.Parched)
            ParchedMode();
        else if (crop_State == cropStates.Destroyed)
            DestroyedMode();
        //Player interaction for harvesting/planting
        if (Input.GetKeyDown(KeyCode.E) && Vector3.Distance(transform.position, player.transform.position) < 2.0f)
            interact = true;
        else
            interact = false;
    }

    //Plant is ready to be planted/start growing
    void ReadyMode()
    {
        //Allows you to interat to plant
        plantStages[currentStage].SetActive(false);
        currentStage = 0;
        TogglePressEText(true, "PLANT 'E'");

        if (interact == true)
        {
            AudioManager.Instance.PlayAudioClip(AudioManager.Instance.plantClip);
            TogglePressEText(false, "");
            elapsed = 0;
            crop_State = cropStates.Growing;
            interact = false;
        }
    }

    //Itterate throught stages of plant until grown
    void GrowingMode()
    {
        plantStages[currentStage].SetActive(true);
        if (StageIncrease())
        {
            if (currentStage < plantStages.Length - 2)
            {
                plantStages[currentStage].SetActive(false);
                currentStage++;
                plantStages[currentStage].SetActive(true);
            }
            else
            {
                crop_State = cropStates.Grown;
                PlantManager.Instance.readyPlants.Add(this);
            }
        }
        //Allows player to water
        //Sets timer that switched to grown when complete
        if (interact == true)
        {
            interact = false;
        }
    }

    //Plant is grown and ready for harvest from player or agent
    void GrownMode()
    {
        plantStages[currentStage].SetActive(false);
        currentStage = 3;
        plantStages[currentStage].SetActive(true);
        TogglePressEText(true, "HARVEST 'E'");
        //Allows the player to harvest to regain health
        if (interact == true)
        {
            AnnihilateThisBiznitch("Player");
        }
    }

    void ParchedMode()
    {
        //Haults Grow timer
        //Starts timer to destroy if not watered in time
    }

    void DestroyedMode()
    {
        //Renders the crop unusable for growing
    }

    //Toggle plant Interaction UI
    private void TogglePressEText(bool value, string t)
    {
        if (pressEText == null) return;
        pressEText.SetText(t);
        pressEText.gameObject.SetActive(value);
    }

    //Remove the plant and apply score -- call agents to recalculate availability
    public void AnnihilateThisBiznitch(string effector)
    {
        TogglePressEText(false, "");
        PlantManager.Instance.RecalculatePlants();
        plantStages[currentStage].SetActive(false);
        currentStage = 0;
        interact = false;
        crop_State = cropStates.Ready;

        if(effector == "Player") GoalManager.Instance.PlayerAddHeld(1);

        AudioManager.Instance.PlayAudioClip(AudioManager.Instance.harvestClip);
        PlantManager.Instance.readyPlants.Remove(this);
    }

    //Increment plant stages
    bool StageIncrease()
    {
        if (elapsed >= stageSpeed)
        {
            elapsed -= stageSpeed;
            return true;
        }
        else
            return false;
    }

    /// <summary>
    /// Get status if plant is able to be harvested
    /// </summary>
    /// <returns>availability of plant</returns>
    public bool GetHarvestState()
    {
        if (crop_State == cropStates.Grown)
            return true;
        else
            return false;
    }

    /// <summary>
    /// Set the ID of this plant
    /// </summary>
    /// <param name="value">ID number</param>
    public void SetID(int value)
    {
        id = value;
    }

    /// <summary>
    /// Returns plants current ID
    /// </summary>
    /// <returns>plant ID</returns>
    public int GetID()
    {
        return id;
    }
}
