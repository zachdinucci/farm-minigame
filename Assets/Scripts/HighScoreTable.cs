﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class HighScoreTable : MonoBehaviour
{
    private Transform entryContainer;
    private Transform entryTemplate;
    private List<HighScoreEntry> highScoreEntryList;
    private List<Transform> highScoreEntryTransformList;

    void Start()
    {
        //Irrelievant in used situation...
    }

    /// <summary>
    /// Reload highscore data, sort data, and set output
    /// </summary>
    public void Initialize()
    {
        entryContainer = transform.Find("highScoreEntryContainer");
        entryTemplate = entryContainer.Find("highScoreEntryTemplate");
        entryTemplate.gameObject.SetActive(false);
        string jsonString = PlayerPrefs.GetString("highScoreTable");
        HighScores highScores = JsonUtility.FromJson<HighScores>(jsonString);

        //Init default score if table null/empty
        if (highScores == null)
        {
            Debug.Log("Initializing table with default values...");
            AddHighScoreEntry(100000000, "00:00:00", 0, 0, 0, "N/A");

            //Reload
            jsonString = PlayerPrefs.GetString("highScoreTable");
            highScores = JsonUtility.FromJson<HighScores>(jsonString);
        }

        //Sort entry list by Score
        for (int i = 0; i < highScores.highScoreEntryList.Count; i++)
        {
            for (int j = 0; j < highScores.highScoreEntryList.Count; j++)
            {
                if (highScores.highScoreEntryList[j].score > highScores.highScoreEntryList[i].score)
                {
                    //Swap
                    HighScoreEntry tmp = highScores.highScoreEntryList[i];
                    highScores.highScoreEntryList[i] = highScores.highScoreEntryList[j];
                    highScores.highScoreEntryList[j] = tmp;
                }
            }
        }
        highScoreEntryTransformList = new List<Transform>();
        foreach (HighScoreEntry h in highScores.highScoreEntryList)
        {
            CreateHighScoreEntryTransform(h, entryContainer, highScoreEntryTransformList);
        }
    }

    private void CreateHighScoreEntryTransform(HighScoreEntry highScoreEntry, Transform container, List<Transform> transformList)
    {
        float templateHeight = 32f;
        Transform entryTransform = Instantiate(entryTemplate, container);
        RectTransform entryRectTransform = entryTransform.GetComponent<RectTransform>();
        entryRectTransform.anchoredPosition = new Vector2(0, -templateHeight * transformList.Count);
        entryTransform.gameObject.SetActive(true);

        //Rank binding
        int rank = transformList.Count + 1;
        string rankString;
        switch (rank)
        {
            case 1: rankString = "1ST"; break;
            case 2: rankString = "2ND"; break;
            case 3: rankString = "3RD"; break;
            default:
                rankString = rank + "TH"; break;
        }

        //Set text based of data
        int _score = highScoreEntry.score;
        entryTransform.Find("posText").GetComponent<TextMeshProUGUI>().SetText(rankString);
        string _time = highScoreEntry.time;
        entryTransform.Find("timeText").GetComponent<TextMeshProUGUI>().SetText(_time);
        int _pScore = highScoreEntry.pScore;
        entryTransform.Find("playerScoreText").GetComponent<TextMeshProUGUI>().SetText(_pScore.ToString());
        int _eScore = highScoreEntry.eScore;
        entryTransform.Find("enemyScoreText").GetComponent<TextMeshProUGUI>().SetText(_eScore.ToString());
        int _eKilled = highScoreEntry.eKilled;
        entryTransform.Find("enemyKilledText").GetComponent<TextMeshProUGUI>().SetText(_eKilled.ToString());
        string _name = highScoreEntry.name;
        entryTransform.Find("nameText").GetComponent<TextMeshProUGUI>().SetText(_name);

        // Set background visible odds and evens, easier to read
        entryTransform.Find("background").gameObject.SetActive(rank % 2 == 1);
        // Highlight First
        if (rank == 1)
        {
            entryTransform.Find("posText").GetComponent<TextMeshProUGUI>().color = new Color32(0,194,0,255);
            entryTransform.Find("timeText").GetComponent<TextMeshProUGUI>().color = new Color32(0, 194, 0, 255);
            entryTransform.Find("playerScoreText").GetComponent<TextMeshProUGUI>().color = new Color32(0, 194, 0, 255);
            entryTransform.Find("enemyScoreText").GetComponent<TextMeshProUGUI>().color = new Color32(0, 194, 0, 255);
            entryTransform.Find("enemyKilledText").GetComponent<TextMeshProUGUI>().color = new Color32(0, 194, 0, 255);
            entryTransform.Find("nameText").GetComponent<TextMeshProUGUI>().color = new Color32(0, 194, 0, 255);
        }

        transformList.Add(entryTransform);
    }

    /// <summary>
    /// Adds a new highscore entry
    /// </summary>
    /// <param name="score"></param>
    /// <param name="time"></param>
    /// <param name="pScore"></param>
    /// <param name="eScore"></param>
    /// <param name="eKill"></param>
    /// <param name="name"></param>
    public void AddHighScoreEntry(int score, string time, int pScore, int eScore, int eKill, string name)
    {
        //Create Entry
        HighScoreEntry highScoreEntry = new HighScoreEntry { score = score, time = time, pScore = pScore, eScore = eScore, eKilled = eKill, name = name };

        //Load Save HighScores
        string jsonString = PlayerPrefs.GetString("highScoreTable");
        HighScores highScores = JsonUtility.FromJson<HighScores>(jsonString);

        if(highScores == null)
        {
            //No stored table, initialize
            highScores = new HighScores()
            {
                highScoreEntryList = new List<HighScoreEntry>()
            };
        }

        //Add new entry
        highScores.highScoreEntryList.Add(highScoreEntry);

        //Save updated
        string json = JsonUtility.ToJson(highScores);
        PlayerPrefs.SetString("highScoreTable", json);
        PlayerPrefs.Save();
    }

    /// <summary>
    /// Resets all PlayerPref Data
    /// </summary>
    public void Reset()
    {
        PlayerPrefs.DeleteAll();
    }

    /// <summary>
    /// Resets all PlayerPref Data and Re-Initialize
    /// </summary>
    public void ResetInitialize()
    {
        Reset();
        Initialize();
    }

    private class HighScores
    {
        public List<HighScoreEntry> highScoreEntryList;
    }

    [System.Serializable]
    /// <summary>
    /// Represents a single high score entry
    /// </summary>
    private class HighScoreEntry
    {
        public int score;
        public string time;
        public int pScore;
        public int eScore;
        public int eKilled;
        public string name;
    }
}


