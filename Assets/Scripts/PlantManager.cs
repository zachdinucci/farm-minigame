﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// Plant Manager Singleton
/// </summary>
public class PlantManager : MonoBehaviour
{
    public static PlantManager Instance { get; private set; } // Static singleton

    [Header("Crops for use: Please Setup")]
    public List<crops> plants;
    [Header("Available Crops: Auto Generated")]
    public List<crops> readyPlants = new List<crops>();

    //Event called by plants when availability changes for agents
    public delegate void PlantRecalculateHandler();
    public event PlantRecalculateHandler PlantRecalculated;

    private void Awake()
    {
        if (Instance == null) { Instance = this; }
        else { Destroy(gameObject); }
    }

    private void Start()
    {
        InitializeID(plants);
    }

    //Generate an ID for each crop
    private void InitializeID(List<crops> value)
    {
        int tempID = 0;
        if (value == null) return;
        foreach (crops x in value)
        {
            x.SetID(tempID);
            tempID++;
        }
    }

    /// <summary>
    /// Return nearest crop component
    /// </summary>
    /// <param name="value">current position</param>
    /// <returns>target position</returns>
    public crops GetNearestHarvestablePlant(Vector3 value)
    {
        crops TempClose = null;
        float MinDistance = float.MaxValue;
        for (int i = 0; i < readyPlants.Count; i++)
        {
            if (Vector3.Distance(value, readyPlants[i].transform.position) < MinDistance)
            {
                MinDistance = Vector3.Distance(value, readyPlants[i].transform.position);
                TempClose = readyPlants[i];
            }
        }
        return TempClose;
    }

    /// <summary>
    /// Returns if there is a harvestable plant
    /// </summary>
    /// <returns>Availability of harvestable plants</returns>
    public bool IsThereAHarvestablePlant()
    {
        if (readyPlants != null && readyPlants.Count >= 1) return true;
        else return false;
    }

    /// <summary>
    /// Call to trigger agents to recalcualte current path if seeking out a plant
    /// </summary>
    public void RecalculatePlants()
    {
        PlantRecalculated?.Invoke();
    }
}
