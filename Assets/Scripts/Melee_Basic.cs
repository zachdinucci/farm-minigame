﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Basic Melee behavior script -- Don't do this :)
/// </summary>
public class Melee_Basic : MonoBehaviour
{
    private Collider col;
    private KeyCode attackButton;
    private float attackDamage = 10f;
    public List<AI> aIInMeleeRange = new List<AI>();

    private void Start()
    {
        col = GetComponent<Collider>();
        attackButton = GetComponentInParent<PlayerControl>().attackButton;
        attackDamage = GetComponentInParent<PlayerControl>().attackDamage;
    }

    private void Update()
    {
        if (Input.GetKeyDown(attackButton))
        {
                Attack();
        }
    }

    //Damage all agents in melee range array if able
    private void Attack()
    {
        if(aIInMeleeRange != null && aIInMeleeRange.Count > 0)
        {
            foreach (AI x in aIInMeleeRange)
            {

                x.TakeDamage(attackDamage);
            }
        }
    }

    //A quick fix to make my problems go away - crumbs under the rug
    private void FixMyLaziness()
    {
        //If she gone she gone - this is a weak way to do this but im a weak boy from school overload *shrug emoji*
        for (var i = aIInMeleeRange.Count - 1; i > -1; i--)
        {
            if (aIInMeleeRange[i] == null)
                aIInMeleeRange.RemoveAt(i);
        }
    }

    //Add agent to an array if in trigger
    private void OnTriggerEnter(Collider other)
    {
        FixMyLaziness();

        if (other.tag == "Enemy")
        {
            aIInMeleeRange.Add(other.gameObject.GetComponent<AI>());
        }
    }

    //Remove agent from array upon leaving
    private void OnTriggerExit(Collider other)
    {
        FixMyLaziness();
        aIInMeleeRange.Remove(other.gameObject.GetComponent<AI>());
    }

}
