﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// DealthPlane script to account for player falling off of map -- respawns player to set location
/// </summary>
public class DeathPlane : MonoBehaviour
{
    public Transform respawnPoint;

    private void OnTriggerEnter(Collider other)
    {
        other.transform.position = respawnPoint.position;
    }

    private void OnCollisionEnter(Collision collision)
    {
        collision.transform.position = respawnPoint.position;
    }
}
