﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public AudioSource gameSounds;

    public AudioClip playerGoalClip;
    public AudioClip enemyGoalClip;
    public AudioClip harvestClip;
    public AudioClip plantClip;

    public static AudioManager Instance;
    // Start is called before the first frame update
    void Start()
    {
        Instance = this;
    }

    public void PlayAudioClip(AudioClip clip)
    {
        gameSounds.PlayOneShot(clip);
        gameSounds.clip = null;
    }
}
