﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Goal script to account for goal collisions
/// </summary>
public class Goal : MonoBehaviour
{
    //If a player or enemy tag collides --> deposit points
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Enemy")
        {
            GoalManager.Instance.EnemyAddScore(other.gameObject.GetComponent<AI>().GetHeldScore());
            other.gameObject.GetComponent<AI>().SetHeldScore(0);
            
        }
        if (other.tag == "Player")
        {
            GoalManager.Instance.DepositPlayerScore();
        }
    }
}
