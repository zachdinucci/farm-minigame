﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Player Controller
/// </summary>
public class PlayerControl : MonoBehaviour
{
    //private CharacterController controller;
    private Animator anim;
    private Vector3 moveDirection = Vector3.zero;
    public Transform bodyMesh;

    public float moveSpeed = 50f;
    public float turnSpeed = 50f;
    public float gravity = 20.0f;
    public float health = 100;
    public float currentHealth;
    public float attackDamage = 50f;
    private Collider col;
    private Rigidbody rb;
    public Collider attackZoneCollider;
    public KeyCode attackButton = KeyCode.Mouse0;

    [Header("Audio Clips")]
    public AudioClip[] attackClip;

    private AudioSource audSource;
    private bool hasDied = false;

    // Start is called before the first frame update
    void Start()
    {
        //controller = GetComponent<CharacterController>();
        col = GetComponent<Collider>();
        rb = GetComponent<Rigidbody>();
        audSource = GetComponent<AudioSource>();
        anim = gameObject.GetComponentInChildren<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if(!hasDied && health <= 0) Die();

        if (!hasDied)
        {
            float moveHorizontal = Input.GetAxis("Horizontal");
            float moveVertical = Input.GetAxis("Vertical");

            Vector3 pos = transform.position;
            pos.x += moveHorizontal * moveSpeed * Time.deltaTime;
            pos.z += moveVertical * moveSpeed * Time.deltaTime;
            transform.position = pos;

            //WASD Rotation/Anim 
            //This script is the reason why bad things exist in the world - never do this - I know this is stinky shit
            if (Input.GetKey(KeyCode.W))
            {
                anim.SetInteger("AnimPar", 1);
                bodyMesh.rotation = Quaternion.Slerp(bodyMesh.rotation, Quaternion.Euler(new Vector3(0, 0, 0)), turnSpeed * Time.deltaTime);
            }
            if (Input.GetKey(KeyCode.A))
            {
                anim.SetInteger("AnimPar", 1);
                bodyMesh.rotation = Quaternion.Slerp(bodyMesh.rotation, Quaternion.Euler(new Vector3(0, 270, 0)), turnSpeed * Time.deltaTime);
            }
            if (Input.GetKey(KeyCode.D))
            {
                anim.SetInteger("AnimPar", 1);
                bodyMesh.rotation = Quaternion.Slerp(bodyMesh.rotation, Quaternion.Euler(new Vector3(0, 90, 0)), turnSpeed * Time.deltaTime);
            }
            if (Input.GetKey(KeyCode.S))
            {
                anim.SetInteger("AnimPar", 1);
                bodyMesh.rotation = Quaternion.Slerp(bodyMesh.rotation, Quaternion.Euler(new Vector3(0, 180, 0)), turnSpeed * Time.deltaTime);
            }
            if (Input.GetKey(KeyCode.S) && Input.GetKey(KeyCode.A))
            {
                anim.SetInteger("AnimPar", 1);
                bodyMesh.rotation = Quaternion.Slerp(bodyMesh.rotation, Quaternion.Euler(new Vector3(0, 225, 0)), turnSpeed * Time.deltaTime);
            }
            if (Input.GetKey(KeyCode.S) && Input.GetKey(KeyCode.D))
            {
                anim.SetInteger("AnimPar", 1);
                bodyMesh.rotation = Quaternion.Slerp(bodyMesh.rotation, Quaternion.Euler(new Vector3(0, 135, 0)), turnSpeed * Time.deltaTime);
            }
            if (Input.GetKey(KeyCode.W) && Input.GetKey(KeyCode.A))
            {
                anim.SetInteger("AnimPar", 1);
                bodyMesh.rotation = Quaternion.Slerp(bodyMesh.rotation, Quaternion.Euler(new Vector3(0, 315, 0)), turnSpeed * Time.deltaTime);
            }
            if (Input.GetKey(KeyCode.W) && Input.GetKey(KeyCode.D))
            {
                anim.SetInteger("AnimPar", 1);
                bodyMesh.rotation = Quaternion.Slerp(bodyMesh.rotation, Quaternion.Euler(new Vector3(0, 45, 0)), turnSpeed * Time.deltaTime);
            }
            if (Input.GetKeyUp(KeyCode.W) || Input.GetKeyUp(KeyCode.A) || Input.GetKeyUp(KeyCode.S) || Input.GetKeyUp(KeyCode.D))
            {
                anim.SetInteger("AnimPar", 0);
            }

            if (Input.GetMouseButtonDown(0))
            {
                PlayRandomAudioClip(attackClip);
                anim.SetBool("isAttacking", true);
            }
            else if (Input.GetMouseButtonUp(0))
            {
                anim.SetBool("isAttacking", false);
            }
            //Forgive me
        }
    }

    private void FixedUpdate()
    {
        //rb.velocity = moveVelocity;
    }

    /// <summary>
    /// Play an audioClip
    /// </summary>
    /// <param name="clip">audio clip</param>
    public void PlayRandomAudioClip(AudioClip[] clip)
    {
        int rand = Random.Range(0, clip.Length);
        AudioClip randClip = clip[rand];
        audSource.PlayOneShot(randClip);
        audSource.clip = null;
    }

    //Check for goal collision for point depositing
    private void OnTriggerEnter(Collider other)
    {

        if (GoalManager.Instance.GetPlantsHeld() > 0 && other.tag == "Goal")
        {
            GoalManager.Instance.PlayerAddScore(GoalManager.Instance.GetPlantsHeld());
            GoalManager.Instance.SetPlayerHeldScore(0);
        }
    }

    /// <summary>
    /// Add health to the player
    /// </summary>
    /// <param name="value">amount to add</param>
    public void AddHealth(float value)
    {
        if (health <= 0) Die();
        health += value;
    }
    /// <summary>
    /// Returns the health of the player
    /// </summary>
    /// <returns>health of player</returns>
    public float GetHealth()
    {
        return health;
    }

    /// <summary>
    /// Returns if Player has died
    /// </summary>
    /// <returns></returns>
    public bool GetHasDied()
    {
        return hasDied;
    }
    /// <summary>
    /// Kill the player
    /// </summary>
    public void Die()
    {
        hasDied = true;
        UIManager.Instance.EndGame();
    }
}
