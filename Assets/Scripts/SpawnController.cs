﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Spawn system behavior script
/// </summary>
public class SpawnController : MonoBehaviour
{
    public enum SpawnState
    {
        SPAWNING,
        WAITING,
        COUNTING
    }

    /// <summary>
    /// Wave component
    /// </summary>
    [System.Serializable]
    public class Wave
    {
        public string name;
        public Transform spawnableAI;
        public int count;
        public float rate;
    }
    [Header("Wave Settings")]
    public Wave[] waves;
    [Header("Spawn Point Positions: Empty GameObjects")]
    public Transform[] spawnPoints;

    private int nextWave = 0;
    public float timeBetweenWaves = 5f;
    private float waveCountdown;
    private float searchCountdown = 1f;

    private SpawnState spawnState = SpawnState.COUNTING;

    private void Start()
    {
        if (spawnPoints.Length == 0)
            Debug.LogError("No spawn points referenced on " + this.gameObject.name);

        waveCountdown = timeBetweenWaves;
    }

    private void Update()
    {
        //If enemies alive - dont spawn/else next wave
        if (spawnState == SpawnState.WAITING)
        {
            if (!IsAIAlive())
            {
                NewWave();
            }
            else
                return;
        }
        //Time to spawn
        if (waveCountdown <= 0)
        {
            if (spawnState != SpawnState.SPAWNING)
            {
                StartCoroutine(SpawnWave(waves[nextWave]));
            }
        }
        else
        {
            waveCountdown -= Time.deltaTime;
        }
    }

    //Itterate waves or loop over
    private void NewWave()
    {
        spawnState = SpawnState.COUNTING;
        waveCountdown = timeBetweenWaves;

        if (nextWave + 1 > waves.Length - 1)
        {
            nextWave = 0;
            //Completed waves --> loop over
        }
        else
            nextWave++;
    }

    //Check if object with tag is still present
    private bool IsAIAlive()
    {
        searchCountdown -= Time.deltaTime;
        if (searchCountdown <= 0f)
        {
            searchCountdown = 1f;
            if (GameObject.FindGameObjectWithTag("Enemy") == null)
                return false;
        }
        return true;
    }

    //Spawns agents based on predetermined rate
    private IEnumerator SpawnWave(Wave _wave)
    {
        spawnState = SpawnState.SPAWNING;

        for (int i = 0; i < _wave.count; i++)
        {
            Spawn(_wave.spawnableAI);
            yield return new WaitForSeconds(1f / _wave.rate);
        }

        spawnState = SpawnState.WAITING;

        yield break;
    }
    
    //Spawn the agent
    void Spawn(Transform _ai)
    {
        Transform tempSpawnPoint = spawnPoints[Random.Range(0, spawnPoints.Length)];
        Instantiate(_ai, tempSpawnPoint.position, tempSpawnPoint.rotation);
    }
}
